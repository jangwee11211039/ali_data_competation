#!/usr/bin/env python
#coding:utf-8
#验证第二弹的思路 


from collections import *
from datetime import *

#解析日期
def parse_date(raw_date):
	date=raw_date.decode("gbk")
	month=int(date[0])
	if len(date)==5:
		day=10*int(date[2])+int(date[3])
	else:
		day=int(date[2])
	return 2013,month,day

#找出周期性东西
def second_mind_expe():
	uid=-1
	bid=-1
	pre_date=-1
	peric_obj=set()
	all_obj=set()
	train_file=open("validation_all.csv")
#	train_file=open("t_alibaba.data.csv")
#	train_file.readline()
#	lines=train_file.readlines()
	#构建重复物品集合
	for line in train_file.readlines():
		line=line.strip().split(",")
		all_obj.add(line[1])
		if uid!=line[0]:
			uid=line[0]
			bid=line[1]
			if int(line[2])==1:
				pre_date=int(line[3])
		else:
			if bid==line[1]:
				if int(line[2])==1:
					if abs(int(line[3])-pre_date)>30:
						peric_obj.add(bid)
			else:
				bid=line[1]
				if int(line[2])==1:
					pre_date=int(line[3])
	train_file.close()	
	
	print len(peric_obj)
	print len(all_obj)

	#构建每个用户的购买集合
	#格式： uid bid（单条）
	all_file=open("t_alibaba_data.csv")
	usr_buy_file=open("usr_buy_item.csv","w")
	all_file.readline()
	uid=-1
	item=set()
	for line in all_file.readlines():
		line=line.strip().split(",")
		if uid!=line[0]:
			item.clear()
			uid=line[0]
			if int(line[2])==1:
				item.add(line[1])
				usr_buy_file.write(",".join(line[:2])+"\n")
		else:
			if int(line[2])==1 and line[1] not in item:
				item.add(line[1])
				usr_buy_file.write(",".join(line[:2])+"\n")

	all_file.close()
	usr_buy_file.close()


	#合并用户的购买集合
	# 格式： uid,bid1,bid2....
	usr_origin_file=open("usr_buy_item.csv")
	usr_merge_file=open("usr_merge_item.txt","w")
	lines=usr_origin_file.readlines()
	lines.sort(key=lambda x:x.split(",")[0])
	for index,line in enumerate(lines):
		uid,bid=line.strip().split(",")
		if  index==0:
			cur_id=uid
			cur_bid=[bid]
		elif cur_id==uid:
			cur_bid.append(bid)
		else:
			usr_merge_file.write(cur_id+"\t"+",".join(set(cur_bid))+"\n")
			cur_id=uid
			cur_bid=[bid]

	usr_origin_file.close()
	usr_merge_file.close()

	#先将周期性物品进行一次购买
	usr_predic_buy=open("usr_pre_buy.txt","w")
	usr_merge_file=open("usr_merge_item.txt")
	for line in usr_merge_file.readlines():
		uid,bid=line.strip().split("\t")
		bid=bid.strip().split(",")
		item=set(bid[:])
		for i in item:
			if i in  peric_obj:
				usr_predic_buy.write(uid+","+i+"\n")
	
	usr_predic_buy.close()
	usr_merge_file.close()

	#寻找上个月买的最好的K个物品
	origin_file=open("t_alibaba_data.csv")
	hot_goods_file=open("hot_goods_file.txt","w")
	origin_file.readline()
	lines=origin_file.readlines()
	lines.sort(key=lambda x:x.strip().split(",")[1])
	bid=-1
	item_dict=dict()
	for line in lines:
		line=line.strip().split(",")
		if int(line[2])==1 and (date(*parse_date(line[3])).month==8 or date(*parse_date(line[3])).month==7):
			if line[1] not in item_dict:
				item_dict[line[1]]=0
			else:
				item_dict[line[1]]+=1
	origin_file.close()

	k=10  #卖出超过K件
	item_list=sorted(item_dict.items(),key=lambda d:d[1],reverse=True)
	for item in item_list:
		if item[1]>k:
			hot_goods_file.write(item[0]+","+str(item[1])+"\n")
	
	hot_goods_file.close()

	#将上个月对热门商品发生过交互的用户提取
	hot_good_set=set()
	hot_good_file=open("hot_goods_file.txt")
	for line in hot_good_file.readlines():
		line=line.strip().split(",")
		hot_good_set.add(line[0])
	origin_file=open("t_alibaba_data.csv")
#	predic2_file=open("predic2_file.txt","w")
	predic2_file=open("usr_pre_buy.txt","a")
	origin_file.readline()
	lines=origin_file.readlines()
	lines.sort(key=lambda x:x.strip().split(",")[0])
	cur_clck=0
	cur_buy=0
	cur_sto=0
	cur_box=0
	for index,line in enumerate(lines):
		line=line.strip().split(",")
		if index==0:
			cur_id=line[0]
			cur_bid=line[1]
		#用户变了 或者 商品变了
		elif cur_id!=line[0] or cur_bid!=line[1]:
			if cur_bid in hot_good_set:
				if (cur_clck>2 or cur_sto>0 or cur_box>0) and cur_buy<4:
					predic2_file.write(cur_id+","+cur_bid+"\n")
			cur_clck=0
			cur_buy=0
			cur_sto=0
			cur_box=0
			cur_id=line[0]
			cur_bid=line[1]
				
		if int(line[2])==0:
			cur_clck+=1
		elif int(line[2])==1:
			cur_buy+=1
		elif int(line[2])==2:
			cur_sto+=1
		elif int(line[2])==3:
			cur_box+=1

	predic2_file.close()

#将上个月买过的商品再买一次
def last_month_buy():
	origin_file=open("t_alibaba_data.csv")
	generate_file=open("usr_pre_buy.txt","a")
	origin_file.readline()
	for line in origin_file.readlines():
		line=line.strip().split(",")
		if	(date(*parse_date(line[3])).month==8) and (int(line[2])>0):
			generate_file.write(line[0]+","+line[1]+"\n")
	
	origin_file.close()
	generate_file.close()


#格式化结果
def generate_result(raw_file):
	origin_file=open(raw_file)
	entrys=origin_file.readlines()
	entrys.sort(key=lambda x:x.strip().split(".")[0])
	result=open("result_10.txt","w")
	for index,entry in enumerate(entrys):
		entry=entry.strip().split(",")
		if index==0:
			cur_id=entry[0]
			cur_bid=[entry[1]]
		elif cur_id==entry[0]:
			cur_bid.append(entry[1])
		else:
			result.write(cur_id+"\t"+",".join(set(cur_bid))+"\n")
			cur_id=entry[0]
			cur_bid=[entry[1]]
	result.close()
	
second_mind_expe()
last_month_buy()
generate_result("usr_pre_buy.txt")

