#!/usr/bin/env python
#coding:utf-8

import random

def last_make():
	origin_file=open("result_10.txt")
	new_file=open("try_my_best_next_will_be_better.txt","w")
	for line in origin_file.readlines():
		new_line=line.strip().split(",")
		rd=random.randint(1,8)
		if (rd==2 or rd==5):
			continue
		if len(new_line)>0 and len(new_line)<=5:
			new_file.write(line)
		elif len(new_line)>5:
			line2=random.sample(new_line[1:],5)
			new_file.write(new_line[0]+",".join("%s"%id for id in line2)+"\n")
	origin_file.close()
	new_file.close()

	
last_make()
